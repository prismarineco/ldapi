# The Universal Loadout System

The Universal Loadout System (a.k.a. `Uniloadout`) is an API designed to make accessing, modifying, and sharing player data really, *really* easy, for both developers and end users. End users will have the choice of either accessing their data via the [PrismarineCo. website](https://prismarine.co) or the desktop app included in this repository. Developers can access the player data by using a player's *uniID*, a unique identifier for every player in the API.

## NOTE

This software is **_extremely_** incomplete. There's a lot of work to be done in order to get this API into a usable state, and even more to get it production-ready. Any and all help that can be provided in furthering that goal is greatly appreciated.

## Building

Building everything can be done via the root-level Makefile in the repository, which will output into a single build folder (`.out` by default, configured by setting `OUTPUT_DIR`). The makefile assumes you'll be building from the repository root.

### Makefile Rules
- `wxf`: Build the desktop frontend for the API. The output is set under `$CXX_OUTPUT_DIR`. Options passed to `configure` and `make` can be specified via `WXF_CONFIGUREOPTS` and `WXF_MAKEOPTS` respectively.
- `server`: Build the API web server. The output is under `$RUST_OUTPUT_DIR/bin`<sup>*</sup>. Options to `cargo` or `rustc` can be set via `SERVER_CARGO_FLAGS` and `SERVER_RUST_FLAGS` respectively.
- `clean`, `clean-wxf`, `clean-server`: Clean all build artifacts, C++-specific artifacts, or Rust-specific artifacts, respectively.
- `all`: Build both the server and the web frontend. (default)

<sup>*: This is a limitation of Cargo. I'd set it to just `$RUST_OUTPUT_DIR` but `--out-dir` is still unstable.</sup>
