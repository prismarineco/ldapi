#!/usr/bin/env python

import sys
import argparse
import subprocess
import os


def main():

    args = get_parser().parse_args(sys.argv[1:])

    if args.subcmd == "server":
        build_uniserver(args)
    elif args.subcmd == "wxgui":
        build_wxfrontend(args)


def build_uniserver(args: argparse.Namespace):

    cargo_cmd: str

    if args.cargo_check:
        cargo_cmd = "check"
    elif args.cargo_clean:
        cargo_cmd = "clean"
    else:
        cargo_cmd = args.cargo_cmd

    cargo_command = ["cargo", f"+{args.rust_toolchain}", cargo_cmd]

    if not args.exclude_manifest:
        cargo_command += ["--manifest-path", args.toplevel + "/uniserver/Cargo.toml"]

    if not args.exclude_target_dir:
        cargo_command += ["--target-dir", rust_outdir(args)]

    subprocess.run(
        cargo_command + args.cargoflags,
        env={"RUSTFLAGS": " ".join(args.rustflags), "MKWD": os.getcwd(), **os.environ},
    )


def build_wxfrontend(args: argparse.Namespace):
    subprocess.run(["autoreconf", "-si", args.toplevel + "/wxfrontend"])
    os.makedirs(cxx_outdir(args), exist_ok=True)
    subprocess.run(
        [args.toplevel + "/wxfrontend/configure"] + args.conflags, cwd=cxx_outdir(args)
    )
    subprocess.run(["make", "-C", cxx_outdir(args)] + args.makeflags)


def get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog=sys.argv[0], description="Build script for the Universal Loadout System"
    )

    parser.add_argument(
        "--outdir",
        help="Set the top level build directory. Defaults to '$(pwd)/.out'.",
        default=(os.getcwd() + "/.out"),
    )

    parser.add_argument(
        "--toplevel",
        help="Set the top source directory (where 'uniserver', 'wxfrontend', etc. are located). Defaults to '$(pwd)'.",
        default=os.getcwd(),
    )

    subp = parser.add_subparsers(help="Build targets", required=True, dest="subcmd")

    server_parser = subp.add_parser("server")
    server_parser.add_argument(
        "--toolchain",
        help="The Rust toolchain to use. Defaults to 'stable'.",
        default="stable",
        dest="rust_toolchain",
    )

    server_parser.add_argument(
        "--rust-arg",
        help="Add arguments to RUSTFLAGS. Defaults to ''.",
        action="append",
        default=[],
        dest="rustflags",
    )

    server_parser.add_argument(
        "--cargo-arg",
        help="Add arguments to the invocation of Cargo. Defaults to ''.",
        action="append",
        default=[],
        dest="cargoflags",
    )

    server_parser.add_argument(
        "--cargo-cmd",
        help="Set the Cargo command to be run. Defaults to 'build'.",
        default="build",
    )

    server_parser.add_argument(
        "--no-manifest",
        help="Don't include the `--manifest-path` option in Cargo's invocation. Defaults to 'false'.",
        action="store_true",
        dest="exclude_manifest",
    )

    server_parser.add_argument(
        "--no-target",
        help="Don't set the target directory for Cargo's invocation. Defaults to 'false'.",
        action="store_true",
        dest="exclude_target_dir",
    )

    server_parser.add_argument(
        "--check",
        "-c",
        help="Shorthand for '--cargo-cmd=check'.",
        action="store_true",
        dest="cargo_check",
    )

    server_parser.add_argument(
        "--clean",
        "-C",
        help="Shorthand for '--cargo-cmd=clean'.",
        action="store_true",
        dest="cargo_clean",
    )

    server_parser.add_argument(
        "--rust-outdir",
        help="Set the build directory for Cargo. Defaults to '$OUTDIR/rust'.",
    )

    wxparser = subp.add_parser("wxgui")
    wxparser.add_argument(
        "--cxx-outdir",
        help="Set the build directory for Autotools. Defaults to '$OUTDIR/cxx'.",
    )

    wxparser.add_argument(
        "--makeflag",
        help="Add arguments to the invocation of 'make'. Defaults to ''.",
        action="append",
        default=[],
        dest="makeflags",
    )

    wxparser.add_argument(
        "--confarg",
        help="Add arguments to the invocation of 'wxfrontend/configure'. Defaults to ''.",
        action="append",
        default=[],
        dest="conflags",
    )

    return parser


def rust_outdir(args: argparse.Namespace) -> str:
    if args.rust_outdir is None:
        return args.outdir + "/rust"
    else:
        return args.rust_outdir


def cxx_outdir(args: argparse.Namespace) -> str:
    if args.cxx_outdir is None:
        return args.outdir + "/cxx"
    else:
        return args.cxx_outdir


if __name__ == "__main__":
    main()
