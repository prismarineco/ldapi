#include <uni_root.hpp>

wxBEGIN_EVENT_TABLE(UniRoot, wxFrame)

EVT_MENU(wxID_EXIT, UniRoot::OnExit)

wxEND_EVENT_TABLE()

UniRoot::UniRoot() : wxFrame(nullptr, 1, "Universal Loadout System") {
    wxMenu* menu_file = new wxMenu();
    menu_file->Append(wxID_EXIT);

    wxMenuBar* menu_bar = new wxMenuBar();
    menu_bar->Append(menu_file, "&File");

    SetMenuBar(menu_bar);
}

UniRoot::~UniRoot() {}

void UniRoot::OnExit(wxCommandEvent& evt) {
    Close(true);
}
