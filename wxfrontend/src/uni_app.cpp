#include <uni_app.hpp>

wxIMPLEMENT_APP(UniApp);

UniApp::UniApp() {}
UniApp::~UniApp() {}

bool UniApp::OnInit() {

    m_rootwin = new UniRoot();
    m_rootwin->Show();

    return true;
};
