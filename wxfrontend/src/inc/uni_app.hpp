#ifndef WXFRONTEND_UNI_APP_HPP_
#define WXFRONTEND_UNI_APP_HPP_

#include <wx/wx.h>
#include <uni_root.hpp>

class UniApp : public wxApp {
  public:

    UniApp();
    ~UniApp();

    virtual bool OnInit();

private:
    UniRoot* m_rootwin = nullptr;
};

#endif // WXFRONTEND_UNI_APP_HPP_
