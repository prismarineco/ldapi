#ifndef WXFRONTEND_UNI_ROOT_HPP_
#define WXFRONTEND_UNI_ROOT_HPP_

#include <wx/wx.h>

class UniRoot : public wxFrame {
  public:
    UniRoot();
    ~UniRoot();

    wxDECLARE_EVENT_TABLE();

    // Event handlers
    void OnExit(wxCommandEvent& evt);
};

#endif // WXFRONTEND_UNI_ROOT_HPP_
