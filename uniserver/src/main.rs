#![cfg_attr(debug_assertions, allow(dead_code))]

extern crate actix_web;
extern crate actix_service;
extern crate serde;
extern crate slen;
extern crate tokio_rustls;
extern crate toml;
extern crate uuid;
extern crate slog;
#[macro_use]
extern crate slog_scope;
extern crate slog_journald;
extern crate slog_async;
extern crate slog_stdlog;
extern crate chrono;
extern crate futures;

/// Various data structures used throughout the API.
mod model;

/// The external routes of the API.
mod route;

/// Logging initialization.
mod log;

mod config;

use actix_web::{web, App, HttpResponse, HttpServer};
use std::{fs, io};
use tokio_rustls::rustls::{
    internal::pemfile::{certs, pkcs8_private_keys},
    NoClientAuth, ServerConfig,
};

#[actix_web::main]
async fn main() -> io::Result<()> {
    let serverconf = config::UniConfig::read("serverconf.toml");

    let log = log::initialize();
    let _g = slog_scope::set_global_logger(log.new(slog::o!("scope" => "global")));
    let _ = slog_stdlog::init();

    let mut ssl_config = ServerConfig::new(NoClientAuth::new());
    let cert_file = &mut io::BufReader::new(fs::File::open(serverconf.ssl.cert).unwrap());
    let key_file = &mut io::BufReader::new(fs::File::open(serverconf.ssl.key).unwrap());
    let cert_chain = certs(cert_file).unwrap();
    let mut keys = pkcs8_private_keys(key_file).unwrap();
    ssl_config
        .set_single_cert(cert_chain, keys.remove(0))
        .unwrap();

    let server = HttpServer::new(move || {
        let mut app = App::new()
            .wrap(log::Logging::new(log.new(slog::o!("scope" => "actix_access"))))
            .configure(route::configure);

        if cfg!(debug_assertions) {
            app = app.service(
                web::resource("/check").route(web::get().to(|| HttpResponse::Ok().finish())),
            )
        }

        app
    });

    println!("Initializing server on {}...", serverconf.bind.addr);
    println!("To see the server logs: journalctl --no-hostname -b 0 -qfp notice _PID={}", std::process::id());

    server
        .bind_rustls(serverconf.bind.addr, ssl_config)?
        .run()
        .await
}
