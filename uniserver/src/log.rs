use slog::Drain;
use slog_journald::JournaldDrain;

use actix_service::{Service, Transform};
use actix_web::{dev::ServiceRequest, dev::ServiceResponse, Error};
use futures::{Future, TryFuture};
use std::boxed::Box;
use std::future::ready;
use std::pin::Pin;
use std::task::Poll;

pub struct Logging {
    logger: slog::Logger,
}

impl Logging {
    pub fn new(logger: slog::Logger) -> Logging {
        Logging { logger }
    }
}

impl<S: 'static, B> Transform<S, ServiceRequest> for Logging
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = LoggingMiddleware<S>;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Transform, Self::InitError>>>>;

    fn new_transform(&self, service: S) -> Self::Future {
        Box::pin(ready(Ok(LoggingMiddleware {
            service,
            logger: self.logger.clone(),
        })))
    }
}

pub struct LoggingMiddleware<S> {
    service: S,
    logger: slog::Logger,
}

impl<S, B> Service<ServiceRequest> for LoggingMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: TryFuture + 'static,
    B: 'static,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future = Pin<Box<dyn futures::Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&self, ctx: &mut std::task::Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(ctx)
    }

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let start_time = chrono::Utc::now();
        let logger = self.logger.clone();
        Box::pin(<Self::Future as futures::TryFutureExt>::inspect_ok(
            Box::pin(self.service.call(req)),
            move |res| {
                let req = res.request().clone();
                let end_time = chrono::Utc::now();
                let duration = end_time - start_time;

                let status = res.status();
                let mut verb = "failed";

                let rs = if status.is_success()
                    || status.is_redirection()
                    || status.is_informational()
                {
                    verb = "completed";
                    slog::record_static!(slog::Level::Info, "")
                } else if status.is_client_error() {
                    slog::record_static!(slog::Level::Warning, "")
                } else {
                    slog::record_static!(slog::Level::Error, "")
                };

                logger.log(&slog::Record::new(
                    &rs,
                    &format_args!(
                        "Request '{} {}' {} with code {} (responded in {}ms)",
                        req.method(),
                        req.path(),
                        verb,
                        status.as_u16(),
                        duration.num_milliseconds()
                    ),
                    slog::b!(
                        "response_time" => duration.num_milliseconds(),
                        "url" => req.uri().to_string(),
                        "route" => req.path().to_string(),
                        "method" => req.method().to_string(),
                        "status_code" => status.as_u16(),
                )));
            },
        ))
    }
}

/// Initialize the server's logging facility.
///
/// Logs from the server are written to `journald`. Querying them can be done via `journalctl`.
pub fn initialize() -> slog::Logger {
    slog::Logger::root(
        JournaldDrain.fuse(),
        slog::o!("version" => env!("CARGO_PKG_VERSION")),
    )
}
