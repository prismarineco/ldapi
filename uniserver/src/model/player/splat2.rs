use super::Loadouts;
use super::StandardRank;
use super::Style;

#[derive(Debug)]
pub struct Info {
    name: String,
    level: u8,
    looks: Look,
    ranks: Ranks,
    loadouts: Loadouts,
}

impl Default for Info {
    fn default() -> Self {
        Self {
            name: "Undefined".into(),
            level: 1,
            looks: Default::default(),
            ranks: Default::default(),
            loadouts: Default::default(),
        }
    }
}

#[derive(Debug)]
pub struct Look {
    species: Species,
    style: Style,
    skin_tone: u8,
    eye_color: u8,
}

impl Default for Look {
    fn default() -> Self {
        Self {
            species: Default::default(),
            style: Default::default(),
            skin_tone: 2,
            eye_color: 0,
        }
    }
}

#[derive(Debug)]
pub enum Species {
    Inkling,
    Octoling,
}

impl Default for Species {
    fn default() -> Self {
        Self::Inkling
    }
}

#[derive(Debug)]
pub struct Ranks {
    splat_zones: StandardRank,
    tower_control: StandardRank,
    rainmaker: StandardRank,
    clam_blitz: StandardRank,
    salmon_run: SalmonRank,
}

impl Default for Ranks {
    fn default() -> Self {
        Self {
            splat_zones: Default::default(),
            tower_control: Default::default(),
            rainmaker: Default::default(),
            clam_blitz: Default::default(),
            salmon_run: Default::default(),
        }
    }
}

#[derive(Debug)]
pub enum SalmonRank {
    Intern,
    Apprentice,
    PartTimer,
    GoGetter,
    Overachiever,
    Profreshional,
}

impl Default for SalmonRank {
    fn default() -> Self {
        Self::Intern
    }
}
