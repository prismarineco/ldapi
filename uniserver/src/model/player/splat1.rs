use super::StandardRank;
use super::Loadouts;
use super::Style;

#[derive(Debug)]
pub struct Info {
    name: String,
    level: u8,
    looks: Look,
    rank: StandardRank,
    loadouts: Loadouts,
}

impl Default for Info {
    fn default() -> Self {
        Self {
            name: "Undefined".into(),
            level: 1,
            looks: Default::default(),
            rank: Default::default(),
            loadouts: Default::default(),
        }
    }
}

#[derive(Debug)]
pub struct Look {
    style: Style,
    skin_tone: u8,
    eye_color: u8,
}

impl Default for Look {
    fn default() -> Self {
        Self {
            style: Default::default(),
            skin_tone: 2,
            eye_color: 6,
        }
    }
}
