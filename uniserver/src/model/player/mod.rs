mod splat1;
mod splat2;

use uuid::Uuid;

#[derive(Debug)]
pub struct Player {
    pub id: Uuid,
    pub splat1: splat1::Info,
    pub splat2: splat2::Info,
}

impl Default for Player {
    fn default() -> Self {
        Self {
            id: Uuid::nil(),
            splat1: Default::default(),
            splat2: Default::default(),
        }
    }
}

#[derive(Debug)]
pub enum Style {
    Male,
    Female,
}

impl Default for Style {
    fn default() -> Self {
        Self::Female
    }
}

#[derive(Debug)]
pub enum StandardRank {
    C(Option<bool>, u8),
    B(Option<bool>, u8),
    A(Option<bool>, u8),
    S(bool, u8),
    X(u16),
}

impl Default for StandardRank {
    fn default() -> Self {
        Self::C(Some(false), 30)
    }
}

#[derive(Debug)]
pub struct Loadouts {
    list: Vec<slen::Loadout>,
    primary: Option<u8>,
}

impl Default for Loadouts {
    fn default() -> Self {
        Self {
            list: vec![],
            primary: None,
        }
    }
}
