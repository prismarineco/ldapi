use actix_web::web;

/// Routes for player-related data and operations.
mod player;

/// Top-level configuration function.
pub fn configure(cfg: &mut web::ServiceConfig) {
    player::configure(cfg);
}
