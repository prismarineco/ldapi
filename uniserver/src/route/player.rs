use actix_web::{
    web::{self, ServiceConfig},
    HttpRequest, HttpResponse,
};

pub fn configure(cfg: &mut ServiceConfig) {
    cfg.service(
        web::scope("/player")
            .service(player_create)
            .service(player_read)
            .service(player_remove),
    );
}

/// Register a player within the database.
#[actix_web::post("/create")]
async fn player_create() -> HttpResponse {
    HttpResponse::NotImplemented().finish()
}

/// Read a player's API entry.
#[actix_web::get("/{uni_id}")]
async fn player_read(uni_id: web::Path<uuid::Uuid>) -> HttpResponse {
    HttpResponse::NotImplemented().finish()
}

/// Remove a player's data from the API.
#[actix_web::post("/{uni_id}/delete")]
async fn player_remove(uni_id: web::Path<uuid::Uuid>) -> HttpResponse {
    HttpResponse::NotImplemented().finish()
}
