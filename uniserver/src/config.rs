use std::io::Read;

use serde::Deserialize;

#[derive(Deserialize)]
pub struct UniConfig {
    pub ssl: Ssl,
    pub bind: Bind,
}

#[derive(Deserialize)]
pub struct Ssl {
    pub key: std::path::PathBuf,
    pub cert: std::path::PathBuf,
}

#[derive(Deserialize)]
pub struct Bind {
    pub addr: std::net::SocketAddr,
}

impl UniConfig {
    pub fn read(path: impl AsRef<std::path::Path>) -> UniConfig {
        let mut file = std::fs::File::open(path).expect("Invalid path");
        let mut content = String::new();
        file.read_to_string(&mut content).unwrap();

        toml::from_str(&content).expect("Invalid config")
    }
}
