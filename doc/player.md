# Player Object Structure

```js

{
    "id": "0b494ca9-5f7d-47bc-9173-dce76150e5b7", // uniID
    "splat1": {
        "name": "DrBluefall", // In game name, capped at 10 characters
        "level": 50, // int, range of 1..=50
        "looks": {
            "style": "Female", // enum, can be Male or Female
            "skin-tone": 3, // int, range of 0..=6, mapped from lightest to darkest
            "eye-color": 3 // int, range of 0..=13
        },
        "rank": "A30", // matches regex: /(([CBA][-+]?)|(S(\+)))\d{2}/i (ex. C00, A-90, S+99) 
        "loadouts": {
            "list": [ ... ], // Array of encoded loadout strings
            "primary": 0 // Index of the user's S1 primary loadout
        }
    },
    "splat2": {
        "name": "YajuShinki", // in game name, 10 character cap,
        "level": {
            "prestige": 2, // Number of times a player has progressed past L99
            "level": 55 // Current level
        },
        "looks": {
            "species": "Inkling", // enum, can be Inkling or Octoling,
            "style": "Female", // enum, can be Male or Female
            "hair": 1, // int, range of 0..=5 if "species" is "Inkling", else 0..=1 if "species" is "Octoling"
            "legwear": 1 // int, range 0..=4
        },
        "ranks": {
            "splat-zones": "C-", // matches regex: /([CBA][-+]?)|(S(\+\d)?)|(X\d{4})/i 
            "tower-control": "X2300",
            "rainmaker": "S+2",
            "clam-blitz": "A",
            "salmon-run": "Profreshional" // enum, can be Intern, Apprentice, Part-Timer, Go-Getter, Overachiever, or Profreshional
        },
        "loadouts": {
            "list": [ ... ], // Array of encoded loadout strings 
            "primary": 0 // Index of the user's primary loadout
        }
    },
    "splat3": { // NOTE: This will not be included in the final API until Splatoon 3 is officially released. What is documented here is just a likely proposal.
        "name": "JanewayFly", // In game name, 10 character cap,
        
        // ... more fields TBD
    }
}

```

## Splatoon 1 Specifics

### Eye Color Map

| Index | Color                                                                                        |
|:-----:|----------------------------------------------------------------------------------------------|
| 0     | ![Eye color 1](https://cdn.wikimg.net/en/splatoonwiki/images/1/1e/S_Customization_Eye_1.png) |
| 1     | ![Eye color 2](https://cdn.wikimg.net/en/splatoonwiki/images/d/d6/S_Customization_Eye_2.png) |
| 2     | ![Eye color 3](https://cdn.wikimg.net/en/splatoonwiki/images/b/b9/S_Customization_Eye_3.png) |
| 3     | ![Eye color 4](https://cdn.wikimg.net/en/splatoonwiki/images/0/0d/S_Customization_Eye_4.png) |
| 4     | ![Eye color 5](https://cdn.wikimg.net/en/splatoonwiki/images/9/95/S_Customization_Eye_5.png) |
| 5     | ![Eye color 6](https://cdn.wikimg.net/en/splatoonwiki/images/9/94/S_Customization_Eye_6.png) |
| 6     | ![Eye color 7](https://cdn.wikimg.net/en/splatoonwiki/images/b/bb/S_Customization_Eye_7.png) |

### Skin Tone Map

| Index | Color                                                                                         |
|:-----:|-----------------------------------------------------------------------------------------------|
| 0     | ![Skin tone 1](https://cdn.wikimg.net/en/splatoonwiki/images/0/0c/S_Customization_Skin_1.png) |
| 1     | ![Skin tone 2](https://cdn.wikimg.net/en/splatoonwiki/images/0/00/S_Customization_Skin_2.png) |
| 2     | ![Skin tone 3](https://cdn.wikimg.net/en/splatoonwiki/images/9/9a/S_Customization_Skin_3.png) |
| 3     | ![Skin tone 4](https://cdn.wikimg.net/en/splatoonwiki/images/8/89/S_Customization_Skin_4.png) |
| 4     | ![Skin tone 5](https://cdn.wikimg.net/en/splatoonwiki/images/c/c3/S_Customization_Skin_5.png) |
| 5     | ![Skin tone 6](https://cdn.wikimg.net/en/splatoonwiki/images/3/35/S_Customization_Skin_6.png) |
| 6     | ![Skin tone 7](https://cdn.wikimg.net/en/splatoonwiki/images/8/8f/S_Customization_Skin_7.png) |

## Splatoon 2 Specifics

### Eye Color Map

| Index | Color                                                                                                                                   |
|:-----:|-----------------------------------------------------------------------------------------------------------------------------------------|
| 0     | ![Eye color 1](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/9/92/S2_Customization_Eye_1.png/120px-S2_Customization_Eye_1.png)    |
| 1     | ![Eye color 2](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/0/0b/S2_Customization_Eye_2.png/120px-S2_Customization_Eye_2.png)    |
| 2     | ![Eye color 3](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/4/49/S2_Customization_Eye_3.png/120px-S2_Customization_Eye_3.png)    |
| 3     | ![Eye color 4](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/5/5e/S2_Customization_Eye_4.png/120px-S2_Customization_Eye_4.png)    |
| 4     | ![Eye color 5](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/9/9f/S2_Customization_Eye_5.png/120px-S2_Customization_Eye_5.png)    |
| 5     | ![Eye color 6](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/e/ea/S2_Customization_Eye_6.png/120px-S2_Customization_Eye_6.png)    |
| 6     | ![Eye color 7](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/f/f7/S2_Customization_Eye_7.png/120px-S2_Customization_Eye_7.png)    |
| 7     | ![Eye color 8](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/1/1a/S2_Customization_Eye_8.png/120px-S2_Customization_Eye_8.png)    |
| 8     | ![Eye color 9](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/c/c8/S2_Customization_Eye_9.png/120px-S2_Customization_Eye_9.png)    |
| 9     | ![Eye color 10](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/5/5e/S2_Customization_Eye_10.png/120px-S2_Customization_Eye_10.png) |
| 10    | ![Eye color 11](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/b/b0/S2_Customization_Eye_11.png/120px-S2_Customization_Eye_11.png) |
| 11    | ![Eye color 12](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/c/ce/S2_Customization_Eye_12.png/120px-S2_Customization_Eye_12.png) |
| 12    | ![Eye color 13](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/0/0b/S2_Customization_Eye_13.png/120px-S2_Customization_Eye_13.png) |
| 13    | ![Eye color 14](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/b/bd/S2_Customization_Eye_14.png/120px-S2_Customization_Eye_14.png) |

### Skin Tone Map

| Index | Color                                                                                                                                  |
|:-----:|----------------------------------------------------------------------------------------------------------------------------------------|
| 0     | ![Skin tone 1](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/b/b3/S2_Customization_Skin_1.png/120px-S2_Customization_Skin_1.png) |
| 1     | ![Skin tone 2](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/f/f7/S2_Customization_Skin_2.png/120px-S2_Customization_Skin_2.png) |
| 2     | ![Skin tone 3](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/4/4b/S2_Customization_Skin_3.png/120px-S2_Customization_Skin_3.png) |
| 3     | ![Skin tone 4](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/4/4d/S2_Customization_Skin_4.png/120px-S2_Customization_Skin_4.png) |
| 4     | ![Skin tone 5](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/b/b4/S2_Customization_Skin_5.png/120px-S2_Customization_Skin_5.png) |
| 5     | ![Skin tone 6](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/5/5d/S2_Customization_Skin_6.png/120px-S2_Customization_Skin_6.png) |
| 6     | ![Skin tone 7](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/a/a1/S2_Customization_Skin_7.png/120px-S2_Customization_Skin_7.png) |

### Hairstyle Map

| Index | `"style": "Female", "species": "Inkling"`                                                                                                                             | `"style": "Male", "species": "Inkling"`                                                                                                                           | `"style": "Female", "species": "Octoling"`                                                                                                                              | `"style": "Male", "species": "Octoling"`                                                                                                                            |
|:-----:|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/8/87/S2_Customization_Inkling_Female_Hair_1_Front.png/120px-S2_Customization_Inkling_Female_Hair_1_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/2/24/S2_Customization_Inkling_Male_Hair_1_Front.png/120px-S2_Customization_Inkling_Male_Hair_1_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/c/cd/S2_Customization_Octoling_Female_Hair_1_Front.png/120px-S2_Customization_Octoling_Female_Hair_1_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/2/28/S2_Customization_Octoling_Male_Hair_1_Front.png/120px-S2_Customization_Octoling_Male_Hair_1_Front.png) |
| 1     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/f/f4/S2_Customization_Inkling_Female_Hair_2_Front.png/120px-S2_Customization_Inkling_Female_Hair_2_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/b/b3/S2_Customization_Inkling_Male_Hair_2_Front.png/120px-S2_Customization_Inkling_Male_Hair_2_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/2/2f/S2_Customization_Octoling_Female_Hair_2_Front.png/120px-S2_Customization_Octoling_Female_Hair_2_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/0/04/S2_Customization_Octoling_Male_Hair_2_Front.png/120px-S2_Customization_Octoling_Male_Hair_2_Front.png) |
| 2     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/d/de/S2_Customization_Inkling_Female_Hair_3_Front.png/118px-S2_Customization_Inkling_Female_Hair_3_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/0/04/S2_Customization_Inkling_Male_Hair_3_Front.png/120px-S2_Customization_Inkling_Male_Hair_3_Front.png) |                                                                                                                                                                         |                                                                                                                                                                     |
| 3     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/3/3e/S2_Customization_Inkling_Female_Hair_4_Front.png/120px-S2_Customization_Inkling_Female_Hair_4_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/8/82/S2_Customization_Inkling_Male_Hair_4_Front.png/120px-S2_Customization_Inkling_Male_Hair_4_Front.png) |                                                                                                                                                                         |                                                                                                                                                                     |
| 4     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/c/ce/S2_Customization_Inkling_Female_Hair_5_Front.png/120px-S2_Customization_Inkling_Female_Hair_5_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/5/50/S2_Customization_Inkling_Male_Hair_5_Front.png/120px-S2_Customization_Inkling_Male_Hair_5_Front.png) |                                                                                                                                                                         |                                                                                                                                                                     |
| 5     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/3/3f/S2_Customization_Inkling_Female_Hair_6_Front.png/120px-S2_Customization_Inkling_Female_Hair_6_Front.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/9/95/S2_Customization_Inkling_Male_Hair_6_Front.png/120px-S2_Customization_Inkling_Male_Hair_6_Front.png) |                                                                                                                                                                         |                                                                                                                                                                     |

### Legwear Map

| Index | `"style": "Female"`                                                                                                                     | `"style": "Male"`                                                                                                                   |
|:-----:|-----------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| 0     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/0/01/S2_Customization_Leg_Female_1.png/100px-S2_Customization_Leg_Female_1.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/8/8d/S2_Customization_Leg_Male_1.png/101px-S2_Customization_Leg_Male_1.png) |
| 1     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/d/d2/S2_Customization_Leg_Female_2.png/100px-S2_Customization_Leg_Female_2.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/6/63/S2_Customization_Leg_Male_2.png/101px-S2_Customization_Leg_Male_2.png) |
| 2     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/8/86/S2_Customization_Leg_Female_3.png/100px-S2_Customization_Leg_Female_3.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/3/35/S2_Customization_Leg_Male_3.png/101px-S2_Customization_Leg_Male_3.png) |
| 3     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/b/b4/S2_Customization_Leg_Female_4.png/100px-S2_Customization_Leg_Female_4.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/d/d8/S2_Customization_Leg_Male_4.png/101px-S2_Customization_Leg_Male_4.png) |
| 4     | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/e/e8/S2_Customization_Leg_Female_5.png/100px-S2_Customization_Leg_Female_5.png) | ![](https://cdn.wikimg.net/en/splatoonwiki/images/thumb/8/89/S2_Customization_Leg_Male_5.png/101px-S2_Customization_Leg_Male_5.png) |
